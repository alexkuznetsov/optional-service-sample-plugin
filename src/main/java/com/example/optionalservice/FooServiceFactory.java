package com.example.optionalservice;

import com.pyxis.greenhopper.GreenHopper;

import org.osgi.framework.BundleContext;

/**
 * A service factory to safely create {@link Foo} instances if and only if {@link GreenHopper} can be found.
 */
public class FooServiceFactory extends OptionalService<GreenHopper>
{
    public FooServiceFactory(final BundleContext bundleContext)
    {
        super(bundleContext, GreenHopper.class);
    }

    public Foo get()
    {
        // If this class is being loaded, that means that a sufficient version of GreenHopper is found.
        // It should be safe to call getService without worrying about the underlying service tracker
        // not finding the GreenHopper instance.
        GreenHopper greenhopper = getService();
        return new Foo(greenhopper);
    }
}
